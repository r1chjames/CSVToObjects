package com.richjames.csvLoader;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

/**
 * Created by Rich on 12/06/2017.
 */
public class main {

    static AmazonDynamoDB amazonDynamoDB;
    static DynamoDBMapper mapper;
    static String accessKey;
    static String secretKey;
    static String tableName;

    public static void main(String[] args) {

        accessKey = args[0];
        secretKey = args[1];
        tableName = args[2];

        amazonDynamoDB = getAmazonDynamoDBClient();
        mapper = new DynamoDBMapper(amazonDynamoDB, getDynamoDbMapperConfig());
        try {
//            processCardLines();
            processAccountLines();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void processCardLines() throws IOException {
        long startTime = System.nanoTime();

        Path file = Paths.get("/home/rich/Downloads/InitialLoad/Card.csv");
        AtomicInteger count = new AtomicInteger(0);
        Stream<String> lines = Files.lines(file, StandardCharsets.UTF_8);
        List<String> failedLines = new ArrayList<>();

        lines.parallel().forEach(line -> {
            String newLine = line.replaceAll("\"", "");
            String[] parts = newLine.split(",");
            AccountDynamoDbModel model = new AccountDynamoDbModel(parts[1], parts[2], false);
            saveToDynamo(model);
            count.incrementAndGet();
        });

        long elapsedTimeInMillis = TimeUnit.MILLISECONDS.convert((System.nanoTime() - startTime), TimeUnit.NANOSECONDS);
        System.out.println(String.format("Completed %s card records in %s milliseconds", count, elapsedTimeInMillis));
        if (failedLines.size() > 0){
            System.out.println(String.format("%s records failed", failedLines.size()));
            Path failedFile = Paths.get("src/main/resources/sampleData/cardFailures.csv");
            Files.write(failedFile, failedLines, Charset.defaultCharset());
        }
    }

    private static void processAccountLines() throws IOException {
        long startTime = System.nanoTime();

        Path file = Paths.get("/home/rich/Downloads/InitialLoad/Customers.csv");
        AtomicInteger count = new AtomicInteger(0);
        Stream<String> lines = Files.lines(file, StandardCharsets.UTF_8);
        List<String> failedLines = new ArrayList<>();

        lines.parallel().forEach(line -> {
            String newLine = line.replaceAll("\"", "");
            String[] parts = newLine.split(",");
            if (parts.length == 2) {
                AccountDynamoDbModel model = loadModelFromDynamoDB(parts[0]);
                if (model != null) {
                    model.setNotificationsOptIn(true);
                    saveToDynamo(model);
                    count.incrementAndGet();
                } else {
                    System.out.println(String.format("Invalid data line: %s", line));
                    failedLines.add(line);
                }
            }
        });

        long elapsedTimeInMillis = TimeUnit.MILLISECONDS.convert((System.nanoTime() - startTime), TimeUnit.NANOSECONDS);
        System.out.println(String.format("Completed %s account records in %s milliseconds", count, elapsedTimeInMillis));
        if (failedLines.size() > 0){
            System.out.println(String.format("%s records failed", failedLines.size()));
            Path failedFile = Paths.get("src/main/resources/sampleData/accountFailures.csv");
            Files.write(failedFile, failedLines, Charset.defaultCharset());
        }
    }

    private static void saveToDynamo(AccountDynamoDbModel model) {
        System.out.println("Processing " + model.toString());
        mapper.save(model);
    }

    private static AmazonDynamoDB getAmazonDynamoDBClient() {
        return AmazonDynamoDBClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey)))
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration("https://dynamodb.eu-west-1.amazonaws.com", "eu-west-1"))
                .build();
    }

    private static DynamoDBMapperConfig getDynamoDbMapperConfig() {
        return DynamoDBMapperConfig
                .builder()
                .withConsistentReads(DynamoDBMapperConfig.ConsistentReads.EVENTUAL)
                .withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tableName))
                .build();
    }

    private static AccountDynamoDbModel loadModelFromDynamoDB(String accountId) {
        Map<String, AttributeValue> queryParams = new HashMap<>();
        queryParams.put(":accountId", new AttributeValue().withS(accountId));

        DynamoDBQueryExpression<AccountDynamoDbModel> queryExpression =
                new DynamoDBQueryExpression<AccountDynamoDbModel>()
                        .withKeyConditionExpression("accountId = :accountId")
                        .withIndexName("accountId-index")
                        .withExpressionAttributeValues(queryParams)
                        .withConsistentRead(false);

        List<AccountDynamoDbModel> model = queryAll(AccountDynamoDbModel.class, queryExpression);
        if (model.isEmpty()) {
            return null;
        }
        return model.get(0);
    }

    private static <T> List<T> queryAll(Class<T> clazz, DynamoDBQueryExpression<T> queryExpression) {
        List<T> all = new ArrayList<>(mapper.query(clazz, queryExpression,
                DynamoDBMapperConfig.PaginationLoadingStrategy.EAGER_LOADING.config()));
        return all;
    }

    private static void clearTable() {
        List<AccountDynamoDbModel> accounts = mapper.scan(AccountDynamoDbModel.class, new DynamoDBScanExpression());
        AtomicInteger count = new AtomicInteger(0);

        accounts.parallelStream().forEach(a -> {
            count.incrementAndGet();
            System.out.println(a.toString());
            System.out.println(count);
            mapper.delete(a);
        });
    }

}
