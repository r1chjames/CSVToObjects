package com.richjames.csvLoader;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIndexHashKey;

import java.time.LocalDateTime;

/**
 * Created by Rich on 07/03/2017.
 */
public class AccountDynamoDbModel {

    @DynamoDBHashKey
    private String card;

    @DynamoDBIndexHashKey(globalSecondaryIndexName = "accountId-index")
    private String accountId;

    @DynamoDBAttribute
    private boolean notificationsOptIn;

    @DynamoDBAttribute
    private String lastUpdated = LocalDateTime.now().toString();

    public AccountDynamoDbModel() {
    }

    public AccountDynamoDbModel(String accountId, String card, boolean notificationsOptIn) {
        this.accountId = accountId;
        this.card = card;
        this.notificationsOptIn = notificationsOptIn;
    }

    public AccountDynamoDbModel(String accountId, String card) {
        this.accountId = accountId;
        this.card = card;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public boolean isNotificationsOptIn() {
        return notificationsOptIn;
    }

    public void setNotificationsOptIn(boolean notificationsOptIn) {
        this.notificationsOptIn = notificationsOptIn;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public String toString() {
        return "{" +
                "card='" + card + '\'' +
                ", accountId='" + accountId + '\'' +
                ", notificationsOptIn=" + notificationsOptIn +
                ", lastUpdated='" + lastUpdated + '\'' +
                '}';
    }
}
